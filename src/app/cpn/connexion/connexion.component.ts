import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY, empty } from 'rxjs';
import {AuthService} from 'src/app/services/cpn/auth.service'
import { TokenStorageService } from 'src/app/services/token-storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  LoginForm: FormGroup;

  constructor(private fb: FormBuilder,private auth:AuthService, private tokenStorage:TokenStorageService) { 
    this.LoginForm = this.fb.group({
      email: [null,[ Validators.required]],
      password: ['', [Validators.required]],

    })
  }

  ngOnInit(): void {

  }


  /************************login *************************/
  onSubmit(){
    const formData = new FormData();
    formData.append( 'email', this.LoginForm.get('email').value );
    formData.append('password', this.LoginForm.get('password').value);
    this.auth.login(formData).subscribe(res=>{    
      if(res.message){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: res.message +' !',
        })
      }
      if(!res.error){
        this.tokenStorage.saveToken(res.data.token);
        this.tokenStorage.saveUser(res.data.user);
    Swal.fire({
      icon: 'success',
      title: 'connecter reussie',
      showConfirmButton: false,
      timer: 6000
    })
    if(res.data.role==="entreprise"){location.href='/cpn/Home_tpe_pme'}
    else if(res.data.role==="agence"){location.href='/cpn/agence'}
    else{location.href='/cpn/Home_collectivite'}
  }
  else{ Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: res.message +' !',
  })}
  },

    error => {
      Swal.fire({
       icon: 'error',
       title: 'Oops...',
       text: 'error 500 !',
     })}
    
    )

  }
}
